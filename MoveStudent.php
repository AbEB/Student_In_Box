<?php

/**
 * Move a student, by changing the value of its field
 */

include_once '../../config.inc.php';
include_once '../../database.inc.php';

$field = htmlspecialchars($_POST["field"]);
$studentId = htmlspecialchars($_POST["studentId"]);
$newValue = htmlspecialchars($_POST["newValue"]);

    if (isset($field) && isset($studentId) && isset($newValue)) 
    {
        $sql = "UPDATE students
                    SET ". $field ." = '". $newValue ."'
                    WHERE students.student_id = '". $studentId ."'";

        // Fonction do_action() do nothing, but prevent DBQuery from crashing.
        function do_action($arg0) {}
        
        DBQuery($sql);
    }
