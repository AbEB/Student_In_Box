<?php

/**
 * Change the value of a field for all students
 */

include_once '../../config.inc.php';
include_once '../../database.inc.php';

$field = htmlspecialchars($_POST["field"]);
$oldValue = htmlspecialchars($_POST["oldValue"]);
$newValue = htmlspecialchars($_POST["newValue"]);

    if (isset($field) && isset($oldValue) && isset($newValue)) 
    {
        $sql = "UPDATE students
                    SET ". $field ." = '". $newValue ."'
                    WHERE students.". $field ." = '". $oldValue ."'";

        // Fonction do_action() do nothing, but prevent DBQuery from crashing
        function do_action($arg0) {}
        
        DBQuery($sql);
    }
