<?php
/**
 * 
 *
 * @package Student_In_Box module
 */

// Add a Menu entry to the Students module.
if ( $RosarioModules['Students'] ) // Verify Students module is activated.
{
	$menu['Students']['admin'] += [
		'Student_In_Box/InBox.php' => dgettext( 'In the box!','Dans la boîte!' ),
	];

	$menu['Students']['teacher'] += [
		'Student_In_Box/InBox.php' => dgettext( 'In the box!','Dans la boîte!' ),
	];
}
/*
if ( $RosarioModules['Etoile'] ) // Verify Etoile module is activated.
{
	$menu['Etoile']['admin'] += [
		'Student_In_Box/InBox.php' => dgettext( 'In the box!','Dans la boîte!' ),
		'Student_In_Box/InBox.php&field_id=220300000' => dgettext( 'In the box!','Dodos' ),
		'Student_In_Box/InBox.php&field_id=220200000' => dgettext( 'In the box!','Arbeit!' ),
	];

	$menu['Etoile']['teacher'] += [
		'Student_In_Box/InBox.php' => dgettext( 'In the box!','Dans la boîte!' ),
	];

}
	*/
