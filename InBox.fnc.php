<?php
/**
 * 
 *
 * @package Student_In_Box
 */

/**
 * Print students grouped by value of $field
 * Allow to change student of box, to change the value of $field
 * 
 * @param text $field field of students
 * 
 * @param bool $all true print also the N/D
 */
function outputInBox( $field, $all )
{
    $query = "SELECT DISTINCT ". $field ." FROM students 
                INNER JOIN student_enrollment ON student_enrollment.student_id = students.student_id
                INNER JOIN school_gradelevels ON school_gradelevels.id = student_enrollment.grade_id
                WHERE student_enrollment.syear = '" . UserSyear() . "'
                    AND (student_enrollment.START_DATE<='" . DBDate() . "'
                    AND (student_enrollment.END_DATE>='" . DBDate() . "' OR student_enrollment.END_DATE IS NULL)) ";
        $query.= ( $all ) ? "" : "AND ". $field ." IS NOT NULL " ;
        $query.= "ORDER BY ". $field ;

    $values_list = DBGet( $query ) ;

    foreach ( $values_list as $id => $value )
    {
        $value = $value[$field];

        $query = " SELECT
                students.student_id AS id,  first_name, middle_name, last_name,
                title as classe,
                school_gradelevels.id as id_classe, school_gradelevels.next_grade_id as id_classe_sup,
                school_gradelevels.short_name as short_name, ". $field ."
                FROM students
                INNER JOIN student_enrollment ON student_enrollment.student_id = students.student_id
                INNER JOIN school_gradelevels ON school_gradelevels.id = student_enrollment.grade_id
                WHERE student_enrollment.syear = '" . UserSyear() . "'
                    AND (student_enrollment.START_DATE<='" . DBDate() . "'
                    AND (student_enrollment.END_DATE>='" . DBDate() . "' OR student_enrollment.END_DATE IS NULL))
                    ";

        $query .= ( $value == null ) 
                ? " AND ". $field ." is null "
                : " AND ". $field ." = '". $value ."'" ;

        $query .= " ORDER BY ".$field.", ID_CLASSE DESC, last_name, first_name";

        $student_RET = DBGet( DBQuery( $query ) );
        
        outputFrame( $value, $student_RET, $field );
    }

    $array_new = array( 1 => array( 'FIRST_NAME' => '+ Glissez ici pour un nouveau champ.' ) );
    
    empty($_REQUEST["_ROSARIO_PDF"]) ? outputFrame( 'Nouveau', $array_new, $field) : true ;
}

/**
 * Print a box with students
 * 
 * @param text $title the title of the box
 * @param array $students the students to print
 * @param text $field e.g. the number of students
 */
function outputFrame( $title, $student_RET, $field = '' )
{
    echo '<div class="'. ( empty($_REQUEST["_ROSARIO_PDF"]) ? 'frame' : 'framePdf' ) .'">';
    
    echo '<div id="'. $title .'" class="box" 
            ondrop="drop(event,\''. $title .'\',\''. $field .'\')" 
            ondragover="allowDrop(event)" >';

    $title = ( empty( $_REQUEST["_ROSARIO_PDF"] ) )
        ?   '<form onsubmit="valueModify(event,\''. $title .'\',\''. $field .'\')" style="margin-bottom:3px">
                    <input type="text" id="in'. $title .'" value="'. $title .'" class="title" >
                    <span class="number">'.count( $student_RET ).'</span>
            </form>'
        :   '<span class="title">'. $title .'</span><span class="number">'. count( $student_RET ) .'</span>'; // TODO css ?
     
    echo    $title . 
            '<table>';

    foreach ( $student_RET as $id => $value )
    {   
        $url = 'Modules.php?modname=Students/Student.php&student_id='.$value['ID'];

        $middleName = $value['MIDDLE_NAME'];

        $middleName .= ( mb_substr( $middleName, -1, mb_strlen( $middleName ) ) !== "'" ) ? ' ' : '';

        $name =  $value['FIRST_NAME'].' '.$middleName.$value['LAST_NAME'] ;

        ?>
            <tr class       = "ligne" id="<?= $value['ID'] ?>"
            	draggable   = "true" ondragstart="drag(event)" 
            	onclick     = "window.open('<?php echo $url; ?>','_blank');">
                <td class="student">
                    <span class="firstN"><?php echo explode(' ', $name, 2)[0] ; ?></span>
                    <span class="name"><?php echo ' '. explode(' ', $name, 2)[1] ; ?></span>
                </td>
                <th class="grade"><?php echo $value['SHORT_NAME']; ?></th>
            </tr>
        <?php
    }
    
    echo '  </table>
          </div>
          </div>';
}
?>

<script type="text/javascript">
/**
 * Functions javascript to modify field value of a student
 */

function allowDrop(ev) {
  ev.preventDefault();  
}

function drag(ev) {
  ev.dataTransfer.setData('text', ev.target.id);
}

function drop( ev, value, field) {
  ev.preventDefault();
  var studentId = ev.dataTransfer.getData('text');
  moveStudent( studentId, value, field );
  $.ajax({
    	url: window.location.href,
    	headers: {
        	"Pragma": "no-cache",
        	"Expires": -1,
        	"Cache-Control": "no-cache"
        	}
    	}).done(function () {
    		window.location.reload(true);
    		});
}

function moveStudent(  studentId, newValue, field ){
	try {            
            var XHR = new XMLHttpRequest();
            var FD  = new FormData(); 
            FD.append("studentId", studentId);
            FD.append("newValue", newValue);
            FD.append("field", field);
            XHR.open("POST", "modules/Student_In_Box/MoveStudent.php", true);
            XHR.send(FD); 
        }
        catch(e) { console.error(e); }
}

/**
 * Javascript function to modify the value of all students who have this value
 */
function valueModify( ev, oldValue, field ){
	try {            
            var XHR = new XMLHttpRequest();
            var FD  = new FormData();
            var newValue = document.getElementById("in"+oldValue).value ;
            FD.append("oldValue", oldValue);
            FD.append("newValue", newValue);
            FD.append("field", field);
            XHR.open("POST", "modules/Student_In_Box/ValueModify.php", true);
            XHR.send(FD); 
        }
        catch(e) { console.error(e); }
        
    $.ajax({                    // Rechargement de la page web. Pourquoi tout ce bazar?
            url: window.location.href,
            headers: {
                "Pragma": "no-cache",
                "Expires": -1,
                "Cache-Control": "no-cache"
            }
        }).done(            
            setTimeout(function(){
            window.location.reload();
            },100) 
        );
}


</script>
