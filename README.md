Student in box Module
==============

http://gitlab.com/AbEB/Student_In_Box

Version 1.0 - April, 2024

License GNU/GPLv2 or later

Author EB

DESCRIPTION
-----------
RosarioSIS module that lets you change the value of a student field simply by drag and drop.

CONTENT
-------
Students
- Student in Box

INSTALL
-------
Copy the `Student_In_Box/` folder (if named `Student_In_Box-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School Setup > School Configuration > Modules_ and click "Activate".

Requires RosarioSIS 6.8+