<?php
/**
 * 
 *
 * @package Student_In_Box
 */

?><link rel="stylesheet" href="modules/Student_In_Box/InBox.css"><?php

require_once 'modules/Student_In_Box/InBox.fnc.php';

$_REQUEST['field_id'] = issetVal( $_REQUEST['field_id'] );

$_REQUEST['all'] = issetVal( $_REQUEST['all'] );


echo '<form action="' . PreparePHP_SELF( $_REQUEST ) . '" method="GET">';

// $select
$fields_RET = DBGet( "SELECT ID,TITLE,SELECT_OPTIONS AS OPTIONS,CATEGORY_ID
    FROM custom_fields
    WHERE TYPE NOT IN ('textarea','text','date','log','holder','files')
    ORDER BY SORT_ORDER IS NULL,SORT_ORDER,TITLE", [], [ 'CATEGORY_ID' ] );

$categories_RET = DBGet( "SELECT ID,TITLE
    FROM student_field_categories", [], [ 'ID' ] );

$select = '<select name="field_id" onchange="ajaxPostForm(this.form,true);">';

$select .= '<option value="">' . _( 'Please choose a student field' ) . '</option>';

$selected = '';

foreach ( (array) $fields_RET as $field_id => $fields )
{
    $select .= '<optgroup label="' . ParseMLField( $categories_RET[ $field_id ][1]['TITLE'] ) . '">';

    foreach ( (array) $fields as $field )
    {
        $selected = '';

        if ( $_REQUEST['field_id'] == $field['ID'] )
        {
            $selected = ' selected';
            $field_title = $field['TITLE'];
        }

        $select .= '<option value="' . AttrEscape( $field['ID'] ) . '"' . $selected . '>' . ParseMLField( $field['TITLE'] ) . '</option>';
    }

    $select .= '</optgroup>';
}

$select .= '</select></form>';

//$all
$wanted_all = ( ! isset( $_REQUEST['all'] ) || $_REQUEST['all'] == ''  ) ;

$all_URL = URLEscape( "Modules.php?modname=" . $_REQUEST['modname'] . '&all=' . $wanted_all . '&field_id='. $_REQUEST['field_id']);

$all_png = $wanted_all ? 'add' : 'remove' ;

$all_button = button( $all_png, 'N/D', $all_URL);

// title
DrawHeader( SchoolInfo( 'TITLE' ) .' - '. $field_title);

if ( empty( $_REQUEST["_ROSARIO_PDF"] ) )
{
    DrawHeader( $select, $all_button, $prefered ) ; 
}
else
{ 
    echo '<div id="date">'. date('d/m/Y') .'</br></div>' ; 
}

if ( isset($_REQUEST['field_id']) && $_REQUEST['field_id'] != '')
{
    outputInBox( 'CUSTOM_'. $_REQUEST['field_id'], $_REQUEST['all'] ) ;
}
